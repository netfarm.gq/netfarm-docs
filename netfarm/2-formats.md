# Netfarm Format

We describe the *Netfarm* format in this document. We use a textual format which
requires very little code to parse, but can represent a large set of data types
with reasonable efficiency.

## Common information

Netfarm works with UTF-8 text, with Unix-style newlines.

## Atoms

An atom is a "word", comprised of non-whitespace characters, possibly prefixed with a set of codecs applied to it.

```bnf
<atom>       ::= <codec-list> <text> | <text>
<text>       ::= <nonconflicting-character>*
<codec-list> ::= <text> ":" | <text> "," <codec-list>
```

(A `nonconflicting-character` is any character other than a colon (`:`) or whitespace.)

Here are some legal atoms:

```
foo:bar
baz
unfrobicate,refrobicate:frobicator
fobert-rripp
```

Here are some illegal atoms (each on its own line):

```
aaa:bbb:ccc
foo bar
```

To allow for more characters, we use a `base64` codec, and other codecs are
used for other data types. You should read [the Codec specification](5-codecs.md)
for information on those.

## Object/Block formats

The "block" is a representation of an object's slots, similar to a list of
pairs in a hash-table. We represent each pair in an object as a two-sided
expression: `key value1 value2 ...`. A key, based on its grouping rules, may
have multiple values, but a key is omitted if there are no values. Keys may only
be strings with no codecs.

### Sections

A block has three or four sections:

- the signature list, a list of signature descriptors,
- metadata, which is a section with *list*-formatted keys,
- data, which is subject to the formatting described by the schema, and
- optionally computed data, which is a section of *list*-formatted keys

#### Signature lists

Signature lists look like typical Netfarm tables, but they do not support
any codecs or grouping, because they only provide an association from keys
to signatures.
A signature list is a list of signature descriptors, separated by newlines.
A signature descriptor is a hexadecimal hash for a user object, and a
base64-encoded signature, such as:

```
8c006d5ffe2d7810474ba8c43039cd1a18f868638c16a625a1e24bba4ee4092b JFKp3AE5fY3M3y5cTAofO3xBlBjMUKpZ9Qjkqhwf5lI=
```

When computing the object's final hash, each signature is decoded into byte vectors.
The vectors are then appended, with the first signature in the object appearing first
in the data to be hashed.

### Grouping in sections

Depending on [the schema used](6-schemas.md), three grouping schemes can be used:

- *single*: only one atom is the value to a slot, disallowing unbound slots
- *list*:   a list of atoms is the value to a slot, preserving order but
  not line breaks, and allowing for no atoms
- *lists*:  a list of atom lines is the value, preserving order and line breaks

Here are some atoms and their in-memory representations (in a pseudo-Lisp
format):

```
single-key Foo
=> (slot-value 'single-key) = Foo

list-key Foo Bar
list-key Baz
--or--
list-key Foo Bar Baz
=> (slot-value 'list-key)   = (Foo Bar Baz)

lists-key Foo Bar
lists-key Baz
=> (slot-value 'lists-key)  = ((Foo Bar) Baz)
```

### Normalized formatting

Normalized formatting is a normalised subset of the Netfarm format which can be
rendered in a deterministic manner. Using non-normalized formatting, keys can
appear in any order, and *list*-grouped keys can have their values grouped in
many different ways, and non-deterministic codecs can generate many different
output atoms.

**Normalized formatting requires some more effort from the renderer:**

- Compression and other non-deterministic codecs are disabled. If these codecs
  would trigger the use of other codecs (such as base64), these are kept.
  For example, the codec list `integer,compress,base64` is reduced to
  `integer,base64`, even though the integer would be more efficiently
  represented without base64. This is to reduce the computation required to
  determine the normalized format, as some kind of "cause and effect" tree would
  be needed otherwise.
- Keys are ordered by name, with the smallest character codes written first.
  We see the correct ordering in Common Lisp's `string<` predicate and Python's
  `<` infix operator, where the first different character is used to decide on
  ordering.
- *line*-ordered values are rendered to one line, and the pair is not rendered
  when there are no values in the list. The lines used in *lines*-ordered values
  are preserved, as these are crucial to correctly parsing the values after they
  have been rendered.

Here is a normalized object:

```
list-key Foo Bar Baz
lists-key Foo Bar
lists-key Baz
single-key Foo
```

The reference renderer takes care to always emit normalized format blocks. 
