# Webs of identities

To our knowledge, it's impossible to give human-friendly names for everyone
to objects without:

- throwing money at names (eg: Namecoin),
- creating race conditions (eg: Nettle)

As a workaround, we use a "web" structure to allow users to resolve names
into hashes through a set of partial maps which contain maps their creators
also trust. Maps are connected to users using the user's `map` slot. (To
prevent circular references from preventing the map from being linked, users
are mutable and the latest map can be found by chasing the latest version
from the next-list.)

## A web format

Maps contain four slots. The first two are a list of object maps in `objects`, 
and a list of category maps in `categories`.
Both lists use the `lists` grouping, but every entry must have exactly
two items.

Object maps contain names and hashes, such as:

```
object foo 0123456789abcdef0123456789abcdef
object bar 54726565576974685261696e636f6174
object baz 5768616c65576974686f757445796573
```

Category maps contain names and categories. These names do not have to be
in the same map, but they are expected to be resolvable somewhere in the
web. One name can belong to multiple categories.

```
category foo example-hash
category foo very-lazy-example
category bar terrible-eno-reference
category baz terrible-eno-reference
```

### Anti-objects

Users can vote "against" names, if they don't have a hash to provide but
do not agree with objects being named. This may be analogous to a down-vote
or a spam report on forums. We introduce the slots `anti-objects` and
`anti-categories`, which have the same syntax as `objects` and `categories`
but serve to diminish the value of objects.

## Resolving names

![A diagram of a Netfarm "web", containing several maps](web-traversal.svg)

When traversing a tree, we assign values to each map we travel on. This
algorithm models an infinite sum of weights, but we terminate when we descend
to some "irrelevent" minimum limit to ensure the algorithm terminates.

1. If the name is in the map we start with, then we can ignore the children and
   use that name. If not, the first map has a value of 1, which we use the
   variable `v` to form new values with.
2. If the name is not in the initial map, we start counting. Let `n` be the
   count of children, and assign a value of `v / n / 2` to each child.
3. When the name is encountered in a map, increment the value of the hash if
   it is found in an `object` clause, or decrement it if it is found in an
   `anti-object` clause, by the map's value (`v`).
4. When the value of the next maps to recurse on are lower than some amount
   (such as 1/20), stop and return the hash with the greatest value.
5. Recurse from step 2 with each child until the condition in step 4 is 
   satisfied.
