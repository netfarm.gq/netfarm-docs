---
title: 'CSS Test'
author: 'Ian Mitchell (Comrade Pingu)'
date: ''
---


<!-- To work with these pieces of metadata, delete the above `---`. -->
<!-- eisvogel.latex
titlepage: [true/false]
toc-ownpage: [true/false]
---

COMPILING COMMANDS: 
```
PDF: pandoc <filename>.md --template=eisvogel --number-sections --toc --listings -o <filename>.pdf

HTML: pandoc -t html5 --self-contained --katex --css .styles/html.css <filename>.md -o <filename>.html
```
You can use webtex or katex. Webtex is much better for drafting, and is *much* quicker to compile; but it only compiles as pictures. Katex compiles as copyable text, which makes it better for a finished product. 

-->

# Testing things

## pretty

### fuckin

#### neato

##### asd;lkfj

###### asd;lfkjdsaf;lkj

Welcome to the NetFarm docs!

*This is where **we** farm* on the ~~net~~ 

* Poopidy scoop

* Woop de scoop

1. This meme isn't funny anymore
	
	(a) fucking stop

2. Ok

$$i\hbar \frac{\partial \psi}{\partial t} = U(t) \psi$$

```C
# include <stdio.h>

int main(){

printf("Hello, world!\n\n");

return 0;

}
```


