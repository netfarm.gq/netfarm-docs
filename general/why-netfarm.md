# Why netfarm?

Nowadays, everyone's in the rush to produce a "decentralized" program which
they hope will replace a large centralized service, and will likely gather
some attention, get a few users and, if they pitched it right, might even
make the news. 

Decentralization is a fad subject, which sits with "Web 3.0" and other 
buzzwords that look nice to managers and make advertisers' work significantly
easier. There are a few moments from where this fad may have came from, such
as the *Cambridge Analytica* leak of Facebook users' data from March 2018 
and the Bitcoin price spike in late 2017 and early 2018. People wanting to make
a quick buck, and users simultaneously with nothing to hid and with an urge
to hide everything collided and made a few projects that probably didn't go too
well compared to their projections. I would be lying through my teeth if I
said I didn't decide to start on one because of reasons other than the fad; but
we would rather find an optimum design for a distributed object store.

## Design

Netfarm doesn't attempt to solve one problem, instead creating a simple layer
which programs can be quickly and correctly be prototyped on. The internal
design is stratified, with a basic protocol for synchronising data according
to other nodes and internal rules on what should be tolerated (cl-decentralise),
cryptographic operations to verify that data (netfarm), and an object store 
which can use the cryptography and data storage facilities to keep a global
state (netfarm/mop). 

We do not see federation as a feasible method for managing global state, as
servers do rely on internal state, such as user accounts, and clients and
other servers communicate with different protocols. Netfarm utilises digital
signatures in absense of a centralised login system, with self-signed keys
utilised to verify that users did indeed create objects. The cl-decentralise
protocol is also very symmetrical and client-node and node-node synchronisation
utilise the same verbs and protocol to send data. cl-decentralise is also mostly
asynchronous, with transactions being able to delivered "out-of-order" and in
parallel over one socket. (Object delivery is not multiplexed, however, we
reserve that synchronous design to simplify the client code.) A
continuation-passing-style interface for Netfarm is possible and will hopefully
become the main method for handling complex transactions.

## The future

A more developed and fully fledged object store could be devloped, allowing
more of a program to be exposed and integrated to the network, ideally creating
a kind of distributed Smalltalk or CLOS system; but this would require a lot of
work to keep it efficient, and would raise questions into how to preserve
enough state to backtrack what an object-oriented program did to get to the
current state and other issues. 

Encryption, and presumably other cryptographic tools, are being backdoored in
perverse ways; they aren't being broken or outlawed, but governments are giving
themselves power to send warrants to server operators that use cryptographic
tools and offer them to users. Some politicians are trying to
[wrap it up in "but our homeland security!"](https://www.lawfareblog.com/principles-more-informed-exceptional-access-debate)
and some are
[outright dumbasses](https://www.newscientist.com/article/2140747-laws-of-mathematics-dont-apply-here-says-australian-pm/).
We hope to also establish protocols and libraries that allow users to hide or
cover up systems like Netfarm which do require some level of interception and
eavesdropping prevention; as arbitrarily listening on and tampering with node
communications could alter a node's perception of the outside world.

Distribution might become more common place so Netfarm, and even this theoretical 
future design, will probably be superseded by a more powerful tool, or the fad 
will die. One #lisp resident asked about fads listed "AI, deep learning, networking, 
web stuff, data-center stuff, [and] autonomous vehicles" as some fads that even 
*carried to research*, so the disappearance of some of those may be a sign that 
Netfarm will also lose support from fad followers. While we can, experiments with 
how to create a better distributed system is needed; most solutions
crawl back into the safe but backwards federated and (*shudder*)
blockchain-based designs.

## What we have now

We have finished cl-decentralise and are working on Netfarm.
Some restructuring has been done and some more is still underway; 
cl-decentralise was recently changed to use generic functions and Netfarm is
being modified to accept a simpler object format after we realised the original
format was unnecessarily complex.

In the meantime, we do publish our programs on 
[GitLab](https://gitlab.com/netfarm.gq) and discuss development on 
`#netfarm` on Freenode and `#netfarm:matrix.org` on Matrix; those will suffice
until we can communicate over Netfarm itself.
