# cl-decentralise2: another library for distributed hacks

cl-decentralise2 is a library that handles the annoying parts of writing a
distributed data system, including synchronisation, wire protocols and 
node discovery. We intend to use cl-decentralise2 as the backbone for our
Netfarm object system, so that Netfarm code can be relieved of handling these
low level details.

## An example of a cl-decentralise2 setup

![A diagram of a typical cl-decentralise2 setup](decentralise2-diagram.svg)

In this diagram, there are two nodes connected to each other over a SSL socket.
Node 1 has a system handling that connection, whereas Node 2's connection is
used by a browser. The same connection protocol code can be used between both
nodes, because the system and browser both respect the 
[connection protocol.](3-connections.md)

Typically Node 2 will be requesting objects from Node 1, and Node 1 will only 
send objects that Node 2 specifically requests. 

Some programs may require some form of streaming, so they may use 
cl-decentralise2's [channel system](4-channels.md) to be notified of new objects
that are in a channel. This can be generalised due to a special "all" channel
that notifies clients of everything; we use this for data synchronisation.
