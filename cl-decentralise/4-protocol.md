# protocol

The protocol is relatively simple. There are only a few things you need to
worry about. The protocol and block names are case insensitive to make it
run better on poor, poor Windows and macOS systems which
[isn't a complaint that only I have](https://plus.google.com/+JunioCHamano/posts/1Bpaj3e3Rru).

You only need to remember four general and two from-server words. Here they are:

- GET: get files
- BLOCK: put/receive files
- ANNOUNCE: log your IP for node discovery
- SUBSCRIBE: subscribe to channels
- ok: everything went well
- error: something went wrong

Here's everything in more detail.

### blocks

A block ends with a line `--EOF--`, so you can't use that line in your program. That's a shame.
A block could look like this:

```
This is a block.
I think it's very nice.
When I wrote this, cl-decentralise had 239 non-empty non-comment lines
  of Common Lisp. That's not very many and I hope I can keep it under
  400 lines with a client.
400 is a very nice round number.
--EOF--
```

### channels

Channels are similar to webhooks in nature, and send blocks on the channel
to anyone subscribed. These can be used for real-time messenging, for example.
They have names like blocks, but these names aren't regulated by cl-decentralise
programs. A general, "all" channel is provided if you want to listen to all
blocks, maybe for snooping activities and node synchronisation.

### `GET` (getting files)

Any number of space-separated block names can be requested on a line, starting
with GET.
A request could look like this:

```
GET foo bar baz
```

Two block names are shadowed by special blocks: `list` and `nodes`.
`list` provides a list of all known blocks, each on their own line.
`nodes` provides a list of announced modes.

Two other names are shadowed to indicate special commands: `BLOCK` and `ANNOUNCE`.

### `BLOCK` (putting files)

`BLOCK (name) (version) (channels*)` indicates a block is going to follow.
The name of the block goes where `(name)` is.
The version, `(version)` is a number which is incremented somehow with
each following version. You can be imaginative with this number, but
Unix timestamps and a simple counter work just as well.
`(channels*)` are names of channels as previously described. A block can
have no channels, which will only be broadcast to "all" listeners.

A block with header might be:

```
BLOCK hi-there 12 channel31 sbs
     X
   XXXXX     This is not
 XXX   XXX   my beautiful house!
XX| ++ |  XX
  | || |
  +----+
--EOF--
```

I hope there are't any other beautiful things the block author hasn't realised aren't theirs.
The node will reply with `ok (name)` if there are no problems,
or `error (name)` and the problem if there is one.

### `SUBSCRIBE` (subscribing to channels)

`SUBSCRIBE (channels*)` tells the node you want to subscribe to some channels.
`(channels*)` can be empty if you don't want any subscriptions, or it can
contain some channels you wish to receive blocks for. I'll remind you that
the special channel "all" sends all blocks over, regardless of channels.

### `ANNOUNCE` (node announcement)

`ANNOUNCE` tells the node it can record your IP and tell other nodes
about it, until you disconnect.
This is important cause you might be using a client which doesn't need all the
information nodes can put out, and you might want to be secret in your doings.

The node won't reply with anything, but you can see your external IP
in their node list. Isn't that nice.

# changing the protocol (somewhat)

The networking and stream code is all done using generic functions, so you can
write a connection subclass which inherits some of the original cl-decentralise
network talk, and replaces the others, or create a new class which does things
completely differently. This may be useful for on-the-wire compression,
implementing binary communication or other experiments. One big advantage this
provides is that you do not have to recreate cl-decentralise to change the
wire protocol.

There are only six generic functions you need to create methods for at most:

- **get-blocks** *connection* *names*: send a `GET` request for all the blocks
  in the list of strings *names* to the connection *connection*. (**get-block**
  is a special case of **get-blocks** which just sends one request over and
  saves you a cons. Wait, no it doesn't, we just make the list. Haha. Maybe it
  looks nicer.)
- **write-block** *connection* *name* *content* &key *version* *channels*: 
  send a `BLOCK` with name *name*, content *content*, version *version* (or 0),
  and channels *channels* (or none) to *connection*.
- **write-error** *connection* *name* &optional *reason*: send an `ERROR` with
  caused by the block named *name*, because of *reason* (or no description is
  reported).
- **write-ok** *connection* *name*: send an `OK` message for the block named
  *name*.
- **subscribe-channels** *connection* *channels*: subscribe to the channels
  *channels*.
- **announce** *connection* *announcep*: announce or suppress the client's
  presence; *announcep* should be truthy (or not present) to announce or
  falsy to suppress.
