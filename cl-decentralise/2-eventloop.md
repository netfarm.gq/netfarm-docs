# event-handler

The event handler is a macro defined in `eventloop.lisp`. If the name isn't
explanatory, the general working principle is:

- Read a request of some form.
- Scan the handler cases to find one which is appropriate for the request.
- Run these requests.

## How does an event loop work?

Usually the process of creating one involves a "handler" object which
is provided some functions to work with, usually lambda expressions.

Thanks to the powers of macros, however, we can safely bind whatever
variables we need in the dispatch, effectively inlining the handlers.

## How do I use *this* event handler tool then?

An event handler looks fairly similar to a `handler-case` expression.
You can glance at the
[Hyperspec](http://www.lispworks.com/documentation/HyperSpec/Body/m_hand_1.htm)
definition here, or read on for an actually useful definition.

An event handler takes the form `(event-loop connection . events)`.
`connection` is a connection object created by cl-decentralise,
and `events` are a set of handlers. All appropriate handlers are
run. Here are the allowed handlers:

- `(:get-blocks (block-list) . body)`: The other node requested some blocks.
  These blocks are bound to `block-list` and all of `body` is evaluated in an
  implicit `progn`. (All handlers follow this bind-and-progn pattern.)
- `(:new-block (name version channels data) . body)`: The node has submitted
  new blocks. All blocks are handled, and each block property (name, version,
  channels, and data) is bound appropriately.
- `((:new-block channel-list) (name version channels data). body)`:
  Similar to `:new-block` but only handles blocks with some channels in
  `channel-list`. This list is not quoted, so you can use any Lisp expression
  to generate or provide this list. `(:new-block ("foo" "bar" "baz"))` will
  land you in some trouble for example; a quote such as in
  `(:new-block '("foo" "bar" "baz"))` is required.
- `(:error (name problem) . body)`: An error has occured on the other node.
  `name` is bound to the name of the block that caused an error, and
  `problem` is the description of the error.
- `(:ok (name) . body)`: A block was accepted. `name` is the name of the block.
- `(:subscribe (channels) . body)`: The other node requested to subscribe to the
  channels in `channels`.
- `(:announce (announcep) . body)`: The other node changed its announcement
  status. `announcep` is bound to the boolean indicating if it should be listed
  as an announced node: `T` if it should and `NIL` if it shouldn't.

## TODO: Continuation-passing style handler!
